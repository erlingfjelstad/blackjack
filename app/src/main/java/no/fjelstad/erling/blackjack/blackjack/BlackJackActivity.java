package no.fjelstad.erling.blackjack.blackjack;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import no.fjelstad.erling.blackjack.R;

public class BlackJackActivity extends AppCompatActivity {

    private static final String RETAIN_FRAGMENT = "arg:BlackJackFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        BlackJackFragment blackJackFragment;

        if (savedInstanceState == null) {
            blackJackFragment = new BlackJackFragment();
            attachFragment(getSupportFragmentManager(), blackJackFragment, RETAIN_FRAGMENT);
        } else {
            // retaining fragment
            blackJackFragment = (BlackJackFragment) getSupportFragmentManager().findFragmentByTag(RETAIN_FRAGMENT);
            attachFragment(getSupportFragmentManager(), blackJackFragment, RETAIN_FRAGMENT);
        }
    }

    private void attachFragment(@NonNull FragmentManager fragmentManager,
                                @NonNull Fragment fragment,
                                @NonNull String tag) {
        fragmentManager
                .beginTransaction()
                .replace(R.id.content_frame, fragment, tag)
                .commit();
    }


}

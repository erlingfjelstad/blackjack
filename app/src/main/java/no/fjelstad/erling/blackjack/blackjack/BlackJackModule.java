package no.fjelstad.erling.blackjack.blackjack;

import dagger.Module;
import dagger.Provides;
import no.fjelstad.erling.blackjack.dagger.FragmentScope;

@Module
public class BlackJackModule {
    @Provides
    @FragmentScope
    public BlackJackPresenter providesBlackJackPresenter() {
        return new BlackJackPresenterImpl();
    }

    @Provides
    @FragmentScope
    public BlackJackRepository providesBlackJackRepository() {
        return new BlackJackRepositoryImpl();
    }

}

package no.fjelstad.erling.blackjack.blackjack;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;
import no.fjelstad.erling.blackjack.App;
import no.fjelstad.erling.blackjack.R;
import no.fjelstad.erling.blackjack.models.BlackJack;
import no.fjelstad.erling.blackjack.models.Player;

public class BlackJackFragment extends Fragment implements BlackJackView {

    @State
    BlackJack blackJack;

    @State
    boolean isShowingDialog;

    @State
    String dialogTitle;

    @State
    String dialogText;

    AlertDialog dialog;

    // views
    @BindView(R.id.recycler_view_dealer)
    RecyclerView recyclerViewDealer;

    @BindView(R.id.recycler_view_player)
    RecyclerView recyclerViewPlayer;

    @BindView(R.id.button_stand)
    AppCompatButton buttonStand;

    @BindView(R.id.button_take_card)
    AppCompatButton buttonTakeCard;

    // strings
    @BindString(R.string.congratulations)
    String congratulations;

    // recyclerview adapters
    private BlackJackAdapter playerBlackJackAdapter;
    private BlackJackAdapter dealerBlackJackAdapter;

    // presenter
    @Inject
    BlackJackPresenter blackJackPresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        setRetainInstance(true);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blackjack, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setUpRecyclerViews();

        if (isShowingDialog) {
            createDialog(dialogTitle, dialogText);
        }

    }


    private void setUpRecyclerViews() {
        Context context = getContext();
        playerBlackJackAdapter = new BlackJackAdapter(context);
        dealerBlackJackAdapter = new BlackJackAdapter(context);

        boolean reverseLayout = false;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, reverseLayout
        );

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(
                context, LinearLayoutManager.HORIZONTAL, reverseLayout
        );
        recyclerViewPlayer.setLayoutManager(linearLayoutManager);
        recyclerViewPlayer.setAdapter(playerBlackJackAdapter);
        recyclerViewDealer.setAdapter(dealerBlackJackAdapter);
        recyclerViewDealer.setLayoutManager(linearLayoutManager1);

        //TODO: DefaultItemAnimator and DiffUtil


    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        // DI
        DaggerBlackJackComponent.builder()
                .appComponent(App.get(getActivity()).appComponent())
                .build()
                .inject(this);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }


    @Override
    public void onResume() {
        super.onResume();
        blackJackPresenter.attach(this);

        blackJackPresenter.deal(blackJack);
    }

    @Override
    public void onPause() {
        super.onPause();
        blackJackPresenter.detach();

        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void renderDealer(@NonNull BlackJack blackJack, @NonNull Player dealer) {
        this.blackJack = blackJack;
        dealerBlackJackAdapter.swapData(dealer.hand().cards());
    }

    @Override
    public void renderPlayer(@NonNull BlackJack blackJack, @NonNull Player player) {
        this.blackJack = blackJack;
        playerBlackJackAdapter.swapData(player.hand().cards());
    }


    @Override
    public void dealerWins(@NonNull BlackJack blackJack) {
        String title = blackJack.dealer().name() + " wins";
        String announcement = congratulations + " " + blackJack.dealer().name() + " points "
                + blackJack.dealer().hand().sum();

        createDialog(title, announcement);
    }

    @Override
    public void playerWins(@NonNull BlackJack blackJack) {
        String title = blackJack.player().name() + " wins";
        String announcement = congratulations + " " + blackJack.player().name() + " points "
                + blackJack.player().hand().sum();

        createDialog(title, announcement);
    }

    private void createDialog(@NonNull String title, @NonNull String announcement) {
        this.dialogTitle = title;
        this.dialogText = announcement;

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(title);
        builder.setMessage(announcement);
        builder.setCancelable(false);
        builder.setPositiveButton("New game", (dialog, which) -> {
            blackJackPresenter.clearTable();
            blackJackPresenter.deal(blackJack);
            isShowingDialog = false;
        });

        dialog = builder.show();

        isShowingDialog = true;
    }

    @Override
    public void playerHasBlackJack(@NonNull BlackJack blackJack) {
        String announcement = congratulations + " " + blackJack.player().name() + " got BlackJack!";
        createDialog("Blackjack!", announcement);
    }

    @Override
    public void dealerHasBlackJack(@NonNull BlackJack blackJack) {
        String announcement = congratulations + " " + blackJack.dealer().name() + " got BlackJack!";
        createDialog("BlackJack!", announcement);
    }

    @OnClick(R.id.button_take_card)
    public void onTakeCardButtonClicked() {
        blackJackPresenter.playerTakeCard();
    }

    @OnClick(R.id.button_stand)
    public void onStandButtonClicked() {
        blackJackPresenter.playerStands();
    }
}

package no.fjelstad.erling.blackjack.models;

public enum State {
    ACTIVE,
    FINISHED
}

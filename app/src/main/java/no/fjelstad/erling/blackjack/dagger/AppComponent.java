package no.fjelstad.erling.blackjack.dagger;

import android.content.Context;
import android.support.annotation.NonNull;

import dagger.Component;

@ApplicationScope
@Component(modules = AppModule.class)
public interface AppComponent {
    @NonNull
    Context context();
}

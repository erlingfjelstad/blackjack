package no.fjelstad.erling.blackjack.models;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

import java.util.Collections;
import java.util.List;

@AutoValue
public abstract class Hand implements Parcelable {

    @NonNull
    public abstract Integer sum();

    @NonNull
    public abstract List<Card> cards();

    @NonNull
    public static Hand create(@NonNull List<Card> cards) {
        Integer sum = 0;
        int cardSize = cards.size();
        for (int i = 0; i < cardSize; i++) {
            sum += cards.get(i).rank().points();
        }

        return new AutoValue_Hand(sum, Collections.unmodifiableList(cards));
    }
}

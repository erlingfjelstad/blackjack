package no.fjelstad.erling.blackjack.dagger;

import javax.inject.Scope;

@Scope
public @interface FragmentScope {
}

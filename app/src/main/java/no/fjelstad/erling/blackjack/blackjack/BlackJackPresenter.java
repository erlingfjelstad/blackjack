package no.fjelstad.erling.blackjack.blackjack;

import android.support.annotation.Nullable;

import no.fjelstad.erling.blackjack.common.Presenter;
import no.fjelstad.erling.blackjack.models.BlackJack;


public interface BlackJackPresenter extends Presenter {
    BlackJack deal(@Nullable BlackJack blackJack);

    void playerTakeCard();

    void playerStands();

    void clearTable();
}

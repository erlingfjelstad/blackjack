package no.fjelstad.erling.blackjack.blackjack;

import android.support.annotation.NonNull;

import no.fjelstad.erling.blackjack.common.View;
import no.fjelstad.erling.blackjack.models.BlackJack;
import no.fjelstad.erling.blackjack.models.Player;

public interface BlackJackView extends View {
    void renderDealer(@NonNull BlackJack blackJack, @NonNull Player dealer);

    void renderPlayer(@NonNull BlackJack blackJack, @NonNull Player player);

    void dealerWins(@NonNull BlackJack blackJack);

    void playerWins(@NonNull BlackJack blackJack);

    void playerHasBlackJack(@NonNull BlackJack blackJack);

    void dealerHasBlackJack(@NonNull BlackJack blackJack);
}

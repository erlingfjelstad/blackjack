package no.fjelstad.erling.blackjack.models;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Card implements Parcelable {

    @NonNull
    public abstract Suit suit();

    @NonNull
    public abstract Rank rank();

    @NonNull
    public static Card create(@NonNull Suit suit, @NonNull Rank rank) {
        return new AutoValue_Card(suit, rank);
    }
}

package no.fjelstad.erling.blackjack.models;

public enum Action {
    HIT,
    STAND,
    INITIAL_DEAL
}

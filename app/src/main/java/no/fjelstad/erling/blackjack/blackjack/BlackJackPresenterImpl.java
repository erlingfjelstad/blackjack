package no.fjelstad.erling.blackjack.blackjack;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;
import no.fjelstad.erling.blackjack.common.View;
import no.fjelstad.erling.blackjack.models.Action;
import no.fjelstad.erling.blackjack.models.BlackJack;
import no.fjelstad.erling.blackjack.models.Deck;
import no.fjelstad.erling.blackjack.models.Hand;
import no.fjelstad.erling.blackjack.models.Player;
import no.fjelstad.erling.blackjack.models.State;

public class BlackJackPresenterImpl implements BlackJackPresenter {
    private final CompositeDisposable disposable;

    private BlackJackView blackJackView;
    private BlackJack blackJack;

    public BlackJackPresenterImpl() {
        disposable = new CompositeDisposable();
    }

    @Override
    public void attach(@NonNull View view) {
        if (view instanceof BlackJackView) {
            blackJackView = (BlackJackView) view;
        }
    }

    @Override
    public void detach() {
        blackJackView = null;
        disposable.clear();
    }

    @Override
    public BlackJack deal(@Nullable BlackJack blackJackState) {
        if (blackJackState == null || State.FINISHED == blackJackState.state()) {
            // create a new game
            Player player = Player.create("Sam", Hand.create(new ArrayList<>()));
            Player dealer = Player.create("Dealer", Hand.create(new ArrayList<>()));

            blackJack = BlackJack.create(player, dealer, Deck.create());

            blackJackView.renderDealer(blackJack, blackJack.dealer());
            blackJackView.renderPlayer(blackJack, blackJack.player());

            if (blackJack.isGameFinished(Action.INITIAL_DEAL)) {
                onGameFinished();
            }
        } else if (State.ACTIVE == blackJackState.state()) {
            // restore state
            blackJack = blackJackState;

            blackJackView.renderDealer(blackJack, blackJack.dealer());
            blackJackView.renderPlayer(blackJack, blackJack.player());

            if (blackJack.isGameFinished(Action.INITIAL_DEAL)) {
                onGameFinished();
            }
        }
        return blackJack;
    }

    @Override
    public void playerTakeCard() {
        blackJack = blackJack.drawCardPlayer();
        blackJackView.renderPlayer(blackJack, blackJack.player());

        if (blackJack.isGameFinished(Action.HIT)) {
            onGameFinished();
        }
    }

    @Override
    public void playerStands() {
        while (blackJack.dealer().hand().sum() <= blackJack.player().hand().sum()) {
            blackJack = blackJack.drawCardDealer();
            blackJackView.renderDealer(blackJack, blackJack.dealer());
        }


        if (blackJack.isGameFinished(Action.STAND)) {
            onGameFinished();
        }
    }

    private void onGameFinished() {
        // finish the game
        blackJack = blackJack.gameFinished();

        // announce the winner
        Player winner = blackJack.announceWinner();
        if (winner == blackJack.player()) {
            if (winner.hand().cards().size() == 2 && winner.hand().sum() == 21) {
                blackJackView.playerHasBlackJack(blackJack);
            } else {
                blackJackView.playerWins(blackJack);
            }

        } else {
            if (winner.hand().cards().size() == 2 && winner.hand().sum() == 21) {
                blackJackView.dealerHasBlackJack(blackJack);
            } else {
                blackJackView.dealerWins(blackJack);
            }
        }

    }

    @Override
    public void clearTable() {
        Player player = Player.create("Sam", Hand.create(new ArrayList<>()));
        Player dealer = Player.create("Dealer", Hand.create(new ArrayList<>()));

        blackJack = BlackJack.create(player, dealer, Deck.create());

        blackJackView.renderDealer(blackJack, dealer);
        blackJackView.renderPlayer(blackJack, player);


    }

}

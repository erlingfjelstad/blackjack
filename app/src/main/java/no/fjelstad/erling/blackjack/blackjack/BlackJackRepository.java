package no.fjelstad.erling.blackjack.blackjack;

import no.fjelstad.erling.blackjack.common.Repository;
import no.fjelstad.erling.blackjack.models.Deck;

interface BlackJackRepository extends Repository {
    Deck getDeck();
}

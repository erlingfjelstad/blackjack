package no.fjelstad.erling.blackjack.common;

public interface Presenter {
    void attach(View view);

    void detach();
}

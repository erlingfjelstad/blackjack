package no.fjelstad.erling.blackjack.models;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.google.auto.value.AutoValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AutoValue
public abstract class BlackJack implements Parcelable {

    private static final int MAX_VALUE = 21;

    @NonNull
    public abstract Player player();

    @NonNull
    public abstract Player dealer();

    @NonNull
    public abstract Deck deck();

    @NonNull
    public abstract State state();

    @NonNull
    public static BlackJack create(@NonNull Player player, @NonNull Player dealer, @NonNull Deck deck) {
        List<Card> deckCards = new ArrayList<>(deck.cards());
        Collections.shuffle(deckCards);

        // player is dealt 2 cards
        List<Card> cardsForPlayer = dealCards(deckCards, 2);
        // dealer is dealt 1 card
        List<Card> cardsForDealer = dealCards(deckCards, 1);

        Player playerCopy = Player.create(player.name(),
                Hand.create(cardsForPlayer));
        Player dealerCopy = Player.create(dealer.name(),
                Hand.create(cardsForDealer));

        return new AutoValue_BlackJack(playerCopy, dealerCopy, Deck.create(deckCards), State.ACTIVE);
    }

    @VisibleForTesting
    @NonNull
    static BlackJack testCreate(@NonNull Player player, @NonNull Player dealer, @NonNull Deck deck) {
        List<Card> deckCards = new ArrayList<>(deck.cards());

        // player is dealt 2 cards
        List<Card> cardsForPlayer = dealCards(deckCards, 2);
        // dealer is dealt 1 card
        List<Card> cardsForDealer = dealCards(deckCards, 1);

        Player playerCopy = Player.create(player.name(),
                Hand.create(cardsForPlayer));
        Player dealerCopy = Player.create(dealer.name(),
                Hand.create(cardsForDealer));

        return new AutoValue_BlackJack(playerCopy, dealerCopy, Deck.create(deckCards), State.ACTIVE);
    }

    @NonNull
    private static List<Card> dealCards(List<Card> deckCards, int amount) {
        if (amount >= deckCards.size()) {
            throw new IllegalStateException("Oops! running out of cards");
        }

        List<Card> cardsForPlayer = new ArrayList<>(amount);

        for (int i = 0; i < amount; i++) {
            cardsForPlayer.add(deckCards.remove(0));
        }

        return cardsForPlayer;
    }

    public boolean isGameFinished(@NonNull Action action) {
        switch (action) {
            case INITIAL_DEAL: {
                return player().hand().sum() == MAX_VALUE;
            }
            case HIT: {
                return player().hand().sum() >= MAX_VALUE || dealer().hand().sum() >= MAX_VALUE;
            }
            case STAND: {
                return dealer().hand().sum() > player().hand().sum();
            }
            default:
                throw new IllegalStateException("Unknown action");
        }

    }

    @NonNull
    public BlackJack gameFinished() {
        return new AutoValue_BlackJack(player(), dealer(), deck(), State.FINISHED);
    }

    @NonNull
    public Player announceWinner() {
        if (player().hand().sum() > MAX_VALUE) {
            // player is busted
            return dealer();
        } else if (dealer().hand().sum() > MAX_VALUE) {
            // dealer is busted
            return player();
        } else if (player().hand().sum() > dealer().hand().sum()) {
            // player has higher sum than dealer
            return player();
        } else {
            return dealer();
        }
    }

    @NonNull
    public BlackJack drawCardPlayer() {
        if (deck().cards().size() < 1) {
            throw new IllegalStateException("Oops, running out of cards");
        }
        List<Card> cards = new ArrayList<>(deck().cards());
        List<Card> playerCards = new ArrayList<>(player().hand().cards());

        playerCards.add(cards.remove(0));

        return new AutoValue_BlackJack(Player.create(player().name(),
                Hand.create(playerCards)), dealer(), Deck.create(cards), State.ACTIVE);
    }

    @NonNull
    public BlackJack drawCardDealer() {
        if (deck().cards().size() < 1) {
            throw new IllegalStateException("Oops, running out of cards");
        }

        List<Card> cards = new ArrayList<>(deck().cards());
        List<Card> dealerCards = new ArrayList<>(dealer().hand().cards());

        dealerCards.add(cards.remove(0));

        return new AutoValue_BlackJack(player(), Player.create(dealer().name(),
                Hand.create(dealerCards)), Deck.create(cards), State.ACTIVE);
    }
}

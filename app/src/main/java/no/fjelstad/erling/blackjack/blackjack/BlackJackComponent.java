package no.fjelstad.erling.blackjack.blackjack;

import dagger.Component;
import no.fjelstad.erling.blackjack.dagger.AppComponent;
import no.fjelstad.erling.blackjack.dagger.FragmentScope;

@FragmentScope
@Component(modules = BlackJackModule.class, dependencies = AppComponent.class)
public interface BlackJackComponent {
    void inject(BlackJackFragment blackJackFragment);
}

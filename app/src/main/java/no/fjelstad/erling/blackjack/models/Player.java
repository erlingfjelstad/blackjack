package no.fjelstad.erling.blackjack.models;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class Player implements Parcelable {

    @NonNull
    public abstract String name();

    @NonNull
    public abstract Hand hand();

    @NonNull
    public static Player create(@NonNull String name, @NonNull Hand hand) {
        return new AutoValue_Player(name, hand);
    }
}

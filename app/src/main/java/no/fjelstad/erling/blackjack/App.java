package no.fjelstad.erling.blackjack;

import android.app.Activity;
import android.app.Application;

import no.fjelstad.erling.blackjack.dagger.AppComponent;
import no.fjelstad.erling.blackjack.dagger.AppModule;
import no.fjelstad.erling.blackjack.dagger.DaggerAppComponent;


public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();

    }
    public AppComponent appComponent() {
        return appComponent;
    }

    public static App get(Activity activity) {
        return (App) activity.getApplication();
    }
}

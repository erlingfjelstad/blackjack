package no.fjelstad.erling.blackjack.blackjack;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import no.fjelstad.erling.blackjack.R;
import no.fjelstad.erling.blackjack.models.Card;

public class BlackJackAdapter extends RecyclerView.Adapter<BlackJackAdapter.ViewHolder> {

    private final Context mContext;
    private final List<Card> list;

    public BlackJackAdapter(Context context) {
        this.mContext = context;
        this.list = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.recycler_view_item_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.update(list.get(position));

    }

    void swapData(@NonNull List<Card> cards) {
        list.clear();
        list.addAll(cards);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_view_card_suit)
        AppCompatImageView imageView;

        @BindView(R.id.text_view_card_rank)
        AppCompatTextView textView;

        //TODO Bind views
        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void update(Card card) {

            textView.setText(card.rank().toString());

            switch (card.suit()) {
                case SPADES: {
                    imageView.setBackgroundResource(R.drawable.spades_icon);
                    break;
                }
                case CLUBS: {
                    imageView.setBackgroundResource(R.drawable.clubs_icon);
                    break;
                }
                case DIAMONDS: {
                    imageView.setBackgroundResource(R.drawable.diamons_icon);
                    break;
                }
                case HEARTS: {
                    imageView.setBackgroundResource(R.drawable.heart_icon);
                    break;
                }
                default:
                    throw new IllegalStateException("Unknown suit");
            }
        }
    }
}
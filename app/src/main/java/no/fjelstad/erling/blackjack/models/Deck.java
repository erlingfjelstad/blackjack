package no.fjelstad.erling.blackjack.models;

import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.auto.value.AutoValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AutoValue
public abstract class Deck implements Parcelable {

    @NonNull
    public abstract List<Card> cards();

    @NonNull
    public static Deck create() {
        int numberOfSuits = Suit.values().length;
        int numberOfRanks = Rank.values().length;

        List<Card> cards = new ArrayList<>();

        for (int i = 0; i < numberOfSuits; i++) {
            for (int j = 0; j < numberOfRanks; j++) {
                cards.add(Card.create(Suit.values()[i], Rank.values()[j]));
            }

        }

        return new AutoValue_Deck(Collections.unmodifiableList(cards));
    }

    @NonNull
    public static Deck create(@NonNull List<Card> cards) {
        return new AutoValue_Deck(Collections.unmodifiableList(cards));
    }

    @NonNull
    public Deck shuffle() {
        List<Card> cards = new ArrayList<>(cards());
        Collections.shuffle(cards);

        return new AutoValue_Deck(Collections.unmodifiableList(cards));
    }
}

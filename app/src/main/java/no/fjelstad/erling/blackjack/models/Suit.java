package no.fjelstad.erling.blackjack.models;

public enum Suit {
    HEARTS,
    CLUBS,
    DIAMONDS,
    SPADES
}

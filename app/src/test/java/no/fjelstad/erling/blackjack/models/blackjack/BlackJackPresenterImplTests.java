package no.fjelstad.erling.blackjack.models.blackjack;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import no.fjelstad.erling.blackjack.blackjack.BlackJackPresenter;
import no.fjelstad.erling.blackjack.blackjack.BlackJackPresenterImpl;
import no.fjelstad.erling.blackjack.blackjack.BlackJackView;
import no.fjelstad.erling.blackjack.models.BlackJack;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(JUnit4.class)
public class BlackJackPresenterImplTests {

    @Mock
    public BlackJackView blackJackView;

    // object to test
    BlackJackPresenter blackJackPresenter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        // setup presenter
        blackJackPresenter = new BlackJackPresenterImpl();
        blackJackPresenter.attach(blackJackView);
    }

    @Test
    public void deal_shouldCreateNewBlackJackGame() throws Exception {
        // passing in null means we create a new game (we have empty state)
        BlackJack blackJack = blackJackPresenter.deal(null);

        // check that view is notified once each for renderPlayer and renderDealer
        verify(blackJackView, times(1)).renderDealer(blackJack, blackJack.dealer());
        verify(blackJackView, times(1)).renderPlayer(blackJack, blackJack.player());

    }

    //TODO: MORE TESTS!
}

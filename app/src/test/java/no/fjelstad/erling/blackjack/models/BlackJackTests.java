package no.fjelstad.erling.blackjack.models;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class BlackJackTests {

    @Mock
    Player player;

    @Mock
    Player dealer;

    @Mock
    Deck deck;

    @Mock
    Card card1, card2, card3, card4;

    @Mock
    Hand hand;

    // object to test
    private BlackJack blackJack;


    @Test
    public void blackJack_shouldConformToContract() throws Exception {
        EqualsVerifier.forClass(BlackJack.class)
                .usingGetClass()
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();

    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        when(player.name()).thenReturn("Player1");
        when(dealer.name()).thenReturn("Player2");
        when(deck.cards()).thenReturn(Arrays.asList(card1, card2, card3, card4));
    }

    @Test
    public void create_shouldCreateBlackJackGame() throws Exception {
        when(card1.rank()).thenReturn(Rank.FIVE);
        when(card2.rank()).thenReturn(Rank.FOUR);
        when(card3.rank()).thenReturn(Rank.THREE);

        blackJack = BlackJack.testCreate(player, dealer, deck);

        // checking that blackJack is created
        assertThat(blackJack).isNotNull();

        // checking that dealer has one card
        assertThat(blackJack.dealer().hand().cards().size()).isEqualTo(1);

        // player should have two cards when game is created
        assertThat(blackJack.player().hand().cards().size()).isEqualTo(2);

        // state should be active
        assertThat(blackJack.state()).isEqualTo(State.ACTIVE);
    }

    @Test
    public void isGameFinished_initialDeal_shouldReturnTrue() throws Exception {
        // we want to simulate that player gets blackjack
        when(card1.rank()).thenReturn(Rank.ACE);
        when(card2.rank()).thenReturn(Rank.KING);
        when(card3.rank()).thenReturn(Rank.TEN);
        when(player.hand()).thenReturn(hand);
        when(hand.cards()).thenReturn(Arrays.asList(card1, card2));

        blackJack = BlackJack.testCreate(player, dealer, deck);
        boolean isGameFinished = blackJack.isGameFinished(Action.INITIAL_DEAL);

        // player should have 21 and game should be finished
        assertThat(isGameFinished).isTrue();
    }

    @Test
    public void isGameFinished_initialDeal_shouldReturnFalse() throws Exception {
        // we want to simulate that player gets a total sum of 10
        when(card1.rank()).thenReturn(Rank.FIVE);
        when(card2.rank()).thenReturn(Rank.FIVE);
        when(card3.rank()).thenReturn(Rank.ACE);
        when(player.hand()).thenReturn(hand);
        when(hand.cards()).thenReturn(Arrays.asList(card1, card2));

        blackJack = BlackJack.testCreate(player, dealer, deck);
        boolean isGameFinished = blackJack.isGameFinished(Action.INITIAL_DEAL);

        // player should have 10 and game should not be finished
        assertThat(isGameFinished).isFalse();

    }

    //TOOD NPE
    @Test
    public void isGameFinished_hit_shouldReturnTrue() throws Exception {
        // we want to simulate that player gets busted
        when(card1.rank()).thenReturn(Rank.KING);
        when(card2.rank()).thenReturn(Rank.TEN);
        when(card3.rank()).thenReturn(Rank.TWO);
        when(card4.rank()).thenReturn(Rank.NINE);
        when(player.hand()).thenReturn(hand);
        when(hand.cards()).thenReturn(Arrays.asList(card1, card2, card4));

        blackJack = BlackJack.testCreate(player, dealer, deck);

        blackJack = blackJack.drawCardPlayer();
        boolean isGameFinished = blackJack.isGameFinished(Action.HIT);

        // player should have been busted and game should be finished
        assertThat(isGameFinished).isTrue();

    }

    @Test
    public void gameFinished_shouldReturnGameOfFinishedState() throws Exception {
        when(card1.rank()).thenReturn(Rank.TWO);
        when(card2.rank()).thenReturn(Rank.THREE);
        when(card3.rank()).thenReturn(Rank.FOUR);
        when(player.hand()).thenReturn(hand);
        when(hand.cards()).thenReturn(Arrays.asList(card1, card2));

        blackJack = BlackJack.testCreate(player, dealer, deck);

        // check that blackJack state is Active when we create it
        assertThat(blackJack.state()).isEqualTo(State.ACTIVE);

        // we want to flip the state of the game to finished
        blackJack = blackJack.gameFinished();

        // check that state is finished
        assertThat(blackJack.state()).isEqualTo(State.FINISHED);
    }

    @Test
    public void announceWinner_shouldAnnouncePlayerToBeWinner() throws Exception {
        when(card1.rank()).thenReturn(Rank.ACE);
        when(card2.rank()).thenReturn(Rank.KING);
        when(card3.rank()).thenReturn(Rank.TWO);
        when(player.hand()).thenReturn(hand);
        when(hand.cards()).thenReturn(Arrays.asList(card1, card2));
        // we simulate that player gets blackjack
        blackJack = BlackJack.testCreate(player, dealer, deck);

        // announce the winner
        Player winner = blackJack.announceWinner();

        // check that player is the winner
        assertThat(blackJack.player()).isEqualTo(winner);
    }

    @Test
    public void drawCardPlayer_shouldDrawNewCard() throws Exception {
        when(card1.rank()).thenReturn(Rank.FOUR);
        when(card2.rank()).thenReturn(Rank.SIX);
        when(card3.rank()).thenReturn(Rank.TWO);
        when(card4.rank()).thenReturn(Rank.TEN);
        when(player.hand()).thenReturn(hand);
        when(hand.cards()).thenReturn(Arrays.asList(card1, card2));

        blackJack = BlackJack.testCreate(player, dealer, deck);

        blackJack = blackJack.drawCardPlayer();

        // check that player has three cards on his hand
        assertThat(blackJack.player().hand().cards().size()).isEqualTo(3);
    }

    @Test
    public void drawCardDealer_shouldDrawNewCard() throws Exception {
        when(card1.rank()).thenReturn(Rank.FOUR);
        when(card2.rank()).thenReturn(Rank.SIX);
        when(card3.rank()).thenReturn(Rank.TWO);
        when(card4.rank()).thenReturn(Rank.TEN);
        when(player.hand()).thenReturn(hand);
        when(hand.cards()).thenReturn(Arrays.asList(card1, card2));

        blackJack = BlackJack.testCreate(player, dealer, deck);

        // initially the dealer will get 1 card on his hand. We want to draw the second one
        blackJack = blackJack.drawCardDealer();

        // check that dealer has two cards on his hand
        assertThat(blackJack.dealer().hand().cards().size()).isEqualTo(2);
    }
}

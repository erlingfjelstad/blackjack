package no.fjelstad.erling.blackjack.models;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

@RunWith(JUnit4.class)
public class DeckTests {

    @Test
    public void deck_shouldConformToContract() throws Exception {
        EqualsVerifier.forClass(Deck.class)
                .usingGetClass()
                .suppress(Warning.INHERITED_DIRECTLY_FROM_OBJECT)
                .verify();

    }
}
